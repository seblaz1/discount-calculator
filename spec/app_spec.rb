require 'spec_helper'

describe 'App' do

    it 'Si recibo 5 unidades a 10 pesos debe dar 50' do
        resultado = `ruby app.rb 10 5`
        expect(resultado).to include '50'
    end

    it 'Si recibo 5 unidades a 10 pesos debe dar 50' do
        resultado = `ruby app.rb 10 4`
        expect(resultado).to include '40'
    end

    it 'si recibo 10 unidades a 100 pesos debe 970' do
        resultado = `ruby app.rb 10 100`
        expect(resultado).to include '970'
    end

    it 'si recibo 10 unidades a 1000 pesos debe 9000' do
        resultado = `ruby app.rb 10 1000`
        expect(resultado).to include '9000'
    end

   
end