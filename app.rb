#!/usr/local/bin/ruby
Dir[File.join(__dir__, 'model', '*.rb')].sort.each { |file| require file }

consola = Consola.new
begin
  parseador = Parseador.new ARGV
  resultado = CalculadoraDescuentos.new.calcular(
    parseador.unit_price,
    parseador.quantity
  )
  Consola.new.imprimir_resultado(resultado)
rescue ArgumentError
  consola.imprimir_error_argumento
end
