class CalculadoraDescuentos
  def calcular(unit_price, quantity)
    bruto = unit_price * quantity
    bruto - calcular_descuento(bruto)
  end

  def calcular_descuento(bruto)
    return bruto * 0.03 if bruto >= 1000 && bruto < 5000
    return bruto * 0.05 if bruto >= 5000 && bruto < 7000
    return bruto * 0.07 if bruto >= 7000 && bruto < 10_000
    return bruto * 0.1 if bruto >= 10_000 && bruto < 50_000
    return bruto * 0.15 if bruto >= 50_000

    0
  end
end
