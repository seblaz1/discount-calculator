class Parseador
  def initialize(argumentos)
    argumentos = validar_argumentos argumentos
    @unit_price = parse_entero(argumentos[0])
    @quantity = parse_entero(argumentos[1])
  end

  attr_reader 'unit_price', 'quantity'

  private

  def parse_entero(cadena)
    entero = cadena.to_i
    (entero.to_s == cadena) || (raise ArgumentError)
    entero
  end

  def validar_argumentos(argumentos)
    raise ArgumentError if argumentos.length != 2

    argumentos
  end
end
